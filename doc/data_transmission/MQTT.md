# **Message queueing telemetry transport(MQTT):**

#### **What is MQTT?**
> #### *Message Queuing Telemetry Transport is an open OASIS and ISO standard lightweight, publish-subscribe network protocol that transports messages between devices. The protocol usually runs over TCP/IP; however, any network protocol that provides ordered, lossless, bi-directional connections can support MQTT. It is designed for connections with remote locations where a "small code footprint" is required or the network bandwidth is limited.*

#### **Fetures of MQTT:**
> * #### *A simple messaging protocol designed for constrained devices with low bandwidth.*
> * #### *A lightweight publish and subscribe system to publish and receive messages as a client.*
> * #### *A perfect solution for IOT applications, also allows to send commands to control outputs, read and publish data from sensor nodes.*
> * #### *Communication between several devices can be established simultaneously.*

#### **A general overview of how communication takes place within MQTT devices:**

> #### *MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.*

> #### **Topics:** 
> #### *are a way to register interest for incoming messages or to specify where to publish the message. Represented by strings, separated by forward slash. Each slash indicates a topic level.*

| Config | Description |
| ------ | ------ |
| MQTT broker url | MQTT broker url |
| Username | MQTT broker username for security (optional) |
| Password | MQTT broker password for security (optional) |
| Client ID | Unique Identification of the client, user can put any name that is convenient.|

#### **The configurations need to be set manually by the user before using the API's**

> #### *Commonly configured parameters*
> #### **For example:**
> #### *A typical mqtt configuration without security will look like this*

```
"mqtt": {
    "brokerUrl": "192.168.0.1:8080",
    "clientID": "shunya pi3",
//Username and Password are optional 
}
```

#### **Shunya interfaces MQTT APIs**
> #### *We will use this APIs to configure and send JSON messages to the MQTT cloud*

| API | Description |
| --- | ----------- |
| newMqtt() | Creates a new MQTT Instance |
| mqttConnect() | Connects to given MQTT broker in the settings |
| mqttPublish() | Publishes the data to the MQTT broker |
| mqttSubscribe() |	Subscribes to the given topic and callback can be used to run a user-defined function when the devices      recieves a message from the topic |
| mqttDisconnect() | Disconnects the MQTT Broker |

#### **Reading data from the device or a sensor:**
> #### *For sending a data we have to collect it first, Fetching data from sensors can be done through Shunya interfaces APIs.*
> #### *Click [Here](https://gitlab.com/akshaykpanchal/shunya-interfaces/-/blob/master/user-docs/shunya-interfaces-API.md) to see APIs for reading sensor data.*

#### **After fetching data we have to convert it into JSON format for sending it over MQTT:**
> #### *Sample data in JSON format is as below* 

```
"Sensor_1":{
             "sensor"   : "Tempreture sensor",
             "timetamp" : 12412214231,
             "tempreture: 39
            }
```

#### **Stpes to publish message on MQTT broker:**

> #### *1. Set payload which is being sent to the broker at perticular topic.*
> #### *2. Configure a connection with MQTT broker (e.g HiveMQTT, Mosquitto)*
> #### *3. Create new MQTT instance for connecting to a broker as per given settings.*
> #### *4. Connect to the broker by passing created instance into an argument.*
> #### *5. Publish a message defined in payload with Topic.*
> #### *6. Release connection with broker by using API.*

> #### *As for now we are using external broker and websocket which provides subscribe option, so we haven't used subscribe or subscribe callback function here*








